**This repo migrated here: https://dev.sect0uch.world/SecT0uch/IFPass/**

# IFPass

IFPass is a python project developped for the "Institut Français en Hongrie" and published under [MIT license](https://framagit.org/SecT0uch/IFPass/blob/master/LICENSE).  
It allows to manage a subscriber database and print member cards.

## Build

1. Install [NSIS](http://nsis.sourceforge.net/Download).
2. Install python 3 and pip.
3. Install the modules with `sudo -H pip install pynsist PyPDF2 termcolor`
4. If pynsist version < 2.4, replace `/usr/lib/python3.*/site-packages/nsist/__init__.py` with https://raw.githubusercontent.com/takluyver/pynsist/master/nsist/__init__.py
5. Run `./build.sh`
